ics-ans-role-ntpclient
===================

Ansible role to install ntpclient.

Requirements
------------

- ansible >= 2.3
- molecule >= 1.24

Role Variables
--------------

```yaml
ntp_enable: true
ntp_servers:
  - 0.pool.ntp.org #use this for now, whilst the hardware gps based ntp server is delivered
  - 1.pool.ntp.org #use this for now, whilst the hardware gps based ntp server is delivered
timezone: Europe/Stockholm
...
```

Example Playbook
----------------

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-ntpclient
```

License
-------

BSD 2-clause
